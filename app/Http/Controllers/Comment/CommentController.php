<?php

namespace App\Http\Controllers\Comment;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\StoreRequest;
use App\Http\Resources\Comment\CommentResource;
use App\Models\Comment;
use App\Models\Post;

class CommentController extends Controller
{
    public function index(Post $post)
    {
        $comments = $post->comments()->get();

        return CommentResource::collection($comments);
    }

    public function store(StoreRequest $request, Post $post)
    {

        $data = $request->validated();

        Comment::create($data);

        $comments = $post->comments()->get();

        return CommentResource::collection($comments);
    }

    public function delete(Comment $comment)
    {
        $comment->delete();

        return \response([]);
    }
}
