<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\StoreRequest;
use App\Http\Requests\Post\UpdateRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Inertia\Response;

class PostController extends Controller
{
    public function index() : Response
    {
        $posts = Post::with(['category','tags'])->withCount('likes')->get()->append(['date']);

        return inertia('Post/Index',compact('posts'));
    }

    public function create() : Response
    {
        $categories = Category::all();
        $tags = Tag::all();

        return inertia('Post/Create',compact('categories','tags'));
    }

    public function edit(Post $post) : Response
    {
        $categories = Category::all();
        $tags = Tag::all();

        return inertia('Post/Edit',compact('post','categories','tags'));
    }

    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        $tagIds = $data['tag_ids'];

        $post = Post::create($data);
        $post->tags()->attach($tagIds);

        return \response(['id' => $post->id]);
    }


    public function show(Post $post)
    {
        $tags = $post->tags()->get();
        $likes = $post->likes()->get();
        $category = $post->category;

        return inertia('Post/Show',compact('post','tags','category','likes'));
    }

    public function update(UpdateRequest $request, Post $post)
    {
        $data = $request->validated();
        $tagIds = $data['tag_ids'];

        unset($data['tag_ids']);

        $post->update($data);
        $post->tags()->sync($tagIds);

        return \response(['id' => $post->id]);
    }

    public function delete(Post $post)
    {
        $post->tags()->detach();

        $post->likes()->delete();
        $post->comments()->delete();

        $post->delete();

        return \response([]);
    }
}
