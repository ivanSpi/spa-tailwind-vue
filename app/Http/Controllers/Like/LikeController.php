<?php

namespace App\Http\Controllers\Like;

use App\Http\Controllers\Controller;
use App\Http\Requests\Like\StoreRequest;
use App\Models\Like;

class LikeController extends Controller
{

    public function store(StoreRequest $request)
    {

        $data = $request->validated();

        if($like = Like::firstWhere($data))
        {
            $like->delete();
            return response([],204);
        }
        Like::create($data);
        return \response([],200);
    }

    public function delete(Like $like)
    {
        $like->delete();

        return \response([]);
    }
}
