<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = ['content','post_id'];

    protected $appends = ['date'];

    /*  ---HELPERS/ATTRIBUTES---  */

    public function getDateAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }


    /*  ---MODEL RELATIONS---  */
    public function post() : BelongsTo
    {
       return $this->belongsTo(Post::class,'post_id','id');
    }

    public function likes() : HasMany
    {
       return $this->hasMany(Like::class,'comment_id','id');
    }

    public function replies() : HasMany
    {
        return $this->hasMany(Reply::class,'comment_id','id');
    }
}
