<?php

namespace App\Http\Controllers\Reply;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\StoreRequest;
use App\Http\Resources\Reply\ReplyResource;
use App\Models\Comment;
use App\Models\Reply;

class ReplyController extends Controller
{
    public function index(Comment $comment)
    {
        $replies = $comment->replies()->get();

        return ReplyResource::collection($replies);
    }

    public function store(StoreRequest $request, Comment $comment)
    {
        $data = $request->validated();

        Reply::create($data);

        $replies= $comment->replies()->get();

        return ReplyResource::collection($replies);
    }

    public function delete(Reply $reply)
    {
        $reply->delete();

        return \response([]);
    }
}
