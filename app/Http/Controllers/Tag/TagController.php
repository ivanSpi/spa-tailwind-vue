<?php

namespace App\Http\Controllers\Tag;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tag\StoreRequest;
use App\Http\Requests\Tag\UpdateRequest;
use App\Models\Tag;
use Inertia\Response;

class TagController extends Controller
{
    public function index() : Response
    {
        $tags = Tag::all();

        return inertia('Tag/Index',compact('tags'));
    }

    public function create() : Response
    {
        return inertia('Tag/Create');
    }

    public function edit(Tag $tag) : Response
    {
        return inertia('Tag/Edit',compact('tag'));
    }

    public function store(StoreRequest $request)
    {

        $data = $request->validated();


        Tag::create($data);


        return \response([]);
    }

    public function update(UpdateRequest $request, Tag $tag)
    {

        $data = $request->validated();

        $tag->update($data);

        return \response([]);
    }

    public function delete(Tag $tag)
    {
        $tag->delete();

        return \response([]);
    }
}
