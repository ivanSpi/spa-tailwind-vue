<?php
namespace App\Http\Controllers;

use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\RegistrationRequest;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{
    public function login()
    {
        return inertia('User/Login');
    }

    public function loginStore(LoginRequest $request)
    {
        $data = $request->validated();

        $user = User::where('email', $data['email'])->first();

        if (Hash::check($data['password'], $user->password)) {
            return response([$user]);
        }

        return $data['password'];
    }

    public function registration()
    {
        return inertia('User/Registration');
    }

    public function registrationStore(RegistrationRequest $request)
    {

        $data = $request->validated();

        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);

        return response([$user]);
    }

    public function signOut(Request $request) {

        $request->session()->flush();

        Auth::logout();

        return redirect('login');
    }
}
