<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Post extends Model
{
    use HasFactory;

    protected $fillable = ['title','content','category_id'];

    protected $appends = ['date'];

    /*  --- HELPERS/ATTRIBUTES ---  */

    public function getDateAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    /*  --- MODEL RELATIONS ---  */
    public function category() : BelongsTo
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function tags() : BelongsToMany
    {
        return $this->belongsToMany(Tag::class,'post_tags','post_id','tag_id');
    }

    public function comments() : HasMany
    {
        return $this->hasMany(Comment::class,'post_id','id');
    }
    public function likes() : HasMany
    {
        return $this->hasMany(Like::class,'post_id','id');
    }
}
