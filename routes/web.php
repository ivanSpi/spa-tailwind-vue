<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'posts'],function () {
    Route::get('/',[\App\Http\Controllers\Post\PostController::class,'index'])->name('post.index');
    Route::get('/create',[\App\Http\Controllers\Post\PostController::class,'create'])->name('post.create');
    Route::post('/',[\App\Http\Controllers\Post\PostController::class,'store'])->name('post.store');
    Route::get('/{post}',[\App\Http\Controllers\Post\PostController::class,'show'])->name('post.show');
    Route::get('/{post}/edit',[\App\Http\Controllers\Post\PostController::class,'edit'])->name('post.edit');
    Route::patch('/{post}',[\App\Http\Controllers\Post\PostController::class,'update'])->name('post.update');
    Route::delete('/{post}',[\App\Http\Controllers\Post\PostController::class,'delete'])->name('post.delete');
});
Route::group(['prefix' => 'categories'],function () {
    Route::get('/',[\App\Http\Controllers\Category\CategoryController::class,'index'])->name('category.index');
    Route::get('/create',[\App\Http\Controllers\Category\CategoryController::class,'create'])->name('category.create');
    Route::post('/',[\App\Http\Controllers\Category\CategoryController::class,'store'])->name('category.store');
    Route::get('/{category}/edit',[\App\Http\Controllers\Category\CategoryController::class,'edit'])->name('category.edit');
    Route::patch('/{category}',[\App\Http\Controllers\Category\CategoryController::class,'update'])->name('category.update');
    Route::delete('/{category}',[\App\Http\Controllers\Category\CategoryController::class,'delete'])->name('category.delete');
});
Route::group(['prefix' => 'tags'],function () {
    Route::get('/',[\App\Http\Controllers\Tag\TagController::class,'index'])->name('tag.index');
    Route::get('/create',[\App\Http\Controllers\Tag\TagController::class,'create'])->name('tag.create');
    Route::post('/',[\App\Http\Controllers\Tag\TagController::class,'store'])->name('tag.store');
    Route::get('/{tag}/edit',[\App\Http\Controllers\Tag\TagController::class,'edit'])->name('tag.edit');
    Route::patch('/{tag}',[\App\Http\Controllers\Tag\TagController::class,'update'])->name('tag.update');
    Route::delete('/{tag}',[\App\Http\Controllers\Tag\TagController::class,'delete'])->name('tag.delete');
});
Route::group(['prefix' => 'comments'],function () {
    Route::get('/{post}',[\App\Http\Controllers\Comment\CommentController::class,'index'])->name('comment.index');
    Route::delete('/{comment}',[\App\Http\Controllers\Comment\CommentController::class,'delete'])->name('comment.delete');
    Route::post('/{post}',[\App\Http\Controllers\Comment\CommentController::class,'store'])->name('comment.store');
});
Route::group(['prefix' => 'likes'],function() {
    Route::post('/',[\App\Http\Controllers\Like\LikeController::class,'store'])->name('like.store');
    Route::delete('/{like}',[\App\Http\Controllers\Like\LikeController::class,'delete'])->name('like.delete');
});
Route::group(['prefix' => 'user'],function() {
    Route::get('/login',[\App\Http\Controllers\AuthController::class,'login'])->name('user.login');
    Route::post('/login',[\App\Http\Controllers\AuthController::class,'loginStore'])->name('user.login.store');
    Route::get('/registration',[\App\Http\Controllers\AuthController::class,'registration'])->name('user.registration');
    Route::post('/registration',[\App\Http\Controllers\AuthController::class,'registrationStore'])->name('user.registration.store');
});
Route::group(['prefix' => 'replies'],function() {
    Route::get('/{comment}',[\App\Http\Controllers\Reply\ReplyController::class,'index'])->name('reply.index');
    Route::post('/{comment}',[\App\Http\Controllers\Reply\ReplyController::class,'store'])->name('reply.store');
    Route::delete('/{reply}',[\App\Http\Controllers\Reply\ReplyController::class,'delete'])->name('reply.delete');
});
