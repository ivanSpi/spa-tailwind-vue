<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Models\Category;
use Inertia\Response;

class CategoryController extends Controller
{
    public function index() : Response
    {
        $categories = Category::all();

        return inertia('Category/Index',compact('categories'));
    }

    public function create() : Response
    {
        return inertia('Category/Create');
    }

    public function edit(Category $category) : Response
    {
        return inertia('Category/Edit',compact('category'));
    }

    public function store(StoreRequest $request)
    {

        $data = $request->validated();


        Category::create($data);


        return \response([]);
    }

    public function update(UpdateRequest $request, Category $category)
    {

        $data = $request->validated();

        $category->update($data);

        return \response([]);
    }

    public function delete(Category $category)
    {
        $category->delete();

        return \response([]);
    }
}
