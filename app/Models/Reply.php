<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Reply extends Model
{
    use HasFactory;

    protected $appends = ['date'];
    protected $fillable = ['comment_id','content'];


    public function getDateAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    public function likes() : HasMany
    {
        return $this->hasMany(Like::class,'reply_id','id');
    }


}
